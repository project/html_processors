<?php

namespace Drupal\html_processors;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Interface for html_gutenberg_processor plugins.
 */
interface HtmlGutenbergProcessorInterface extends ConfigurableInterface, PluginFormInterface {

  /**
   * Returns the plugin label.
   *
   * @return string
   *   The label.
   */
  public function label();

  /**
   * Process the HTML tag to convert it to Gutenberg.
   *
   * @param \DOMDocument $source
   *   The source HTML.
   */
  public function process(\DOMDocument &$source);

  /**
   * Get a particular configuration value.
   *
   * @param string $key
   *   Key of the configuration.
   *
   * @return mixed|null
   *   Setting value if found.
   */
  public function getSetting($key);

}
