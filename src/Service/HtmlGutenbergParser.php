<?php

namespace Drupal\html_processors\Service;

use Drupal\html_processors\HtmlGutenbergProcessorManager;
use Drupal\html_processors\HtmlGutenbergProcessorBase;

/**
 * Service to parse HTML to Gutenberg.
 */
class HtmlGutenbergParser {

  /**
   * The plugin.manager.html_gutenberg_processor service.
   *
   * @var \Drupal\html_processors\HtmlGutenbergProcessorManager
   */
  protected $htmlGutenbergProcessor;

  /**
   * Constructs a HtmlGutenbergParser object.
   *
   * @param \Drupal\html_processors\HtmlGutenbergProcessorManager $html_gutenberg_processor
   *   The plugin.manager.html_gutenberg_processor service.
   */
  public function __construct(HtmlGutenbergProcessorManager $html_gutenberg_processor) {
    $this->htmlGutenbergProcessor = $html_gutenberg_processor;
  }

  /**
   * Parses HTML and converts it to Gutenberg.
   *
   * @param string $data
   *   The original HTML.
   * @param array $processors_config
   *   Options passed to the processors keyed by processor id.
   *
   * @return string
   *   The processed HTML.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function parse($data, array $processors_config = []) {
    // Load the HTML as a DOM document to traverse through.
    $source = new \DOMDocument('5.0', 'UTF-8');
    // Remove comments.
    $data = preg_replace('/<!--(.|\s)*?-->/', '', $data);
    // Replace &nbsp; to space.
    $data = str_replace("&nbsp;", " ", $data);
    try {
      @$source->loadHTML(mb_convert_encoding($data, 'HTML-ENTITIES', 'UTF-8'));
    }
    catch (\Exception $th) {
      // Skipping malformed HTML.
    }
    $source->encoding = 'utf-8';

    // Load and process gutenberg processor plugins.
    $processors = $this->htmlGutenbergProcessor->getSortedDefinitions();
    foreach ($processors as $plugin_id => $plugin_definition) {
      $processor_config = $processors_config[$plugin_id] ?? [];
      // Skip if disabled.
      if ($processor_config[HtmlGutenbergProcessorBase::DISABLED]) {
        continue;
      }
      $processor = $this->htmlGutenbergProcessor->createInstance($plugin_id, $processor_config);
      $processor->process($source);
    }

    // Removing unneeded HTML parts.
    $result = preg_replace('/<!DOCTYPE .*>/', '', $source->saveHTML());
    $trim_off_front = strlen('<html><body>') + 1;
    $trim_off_end = (strrpos($result, '</body></html>')) - strlen($result);
    $result = substr($result, $trim_off_front, $trim_off_end);
    // Remove empty tags and spaces between tags.
    $result = preg_replace('/<([^<\/>]*)>([\s]*?|(?R))<\/\1>/imsU', '', $result);
    $result = preg_replace("/>\s+</", "><", $result);
    return $result;
  }

  /**
   * Returns the Gutenberg processor manager.
   *
   * @return \Drupal\html_processors\HtmlGutenbergProcessorManager
   *   The Gutenberg processor manager.
   */
  public function getGutenbergProcessorManager() {
    return $this->htmlGutenbergProcessor;
  }

}
