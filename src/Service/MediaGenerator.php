<?php

namespace Drupal\html_processors\Service;

use Drupal\media\Entity\Media;
use Drupal\Core\Utility\Token;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\FileRepositoryInterface;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Exception\TransferException;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\Exception\InvalidStreamWrapperException;
use GuzzleHttp\Client;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Service to generate media entities from remote files.
 */
class MediaGenerator {

  use DependencySerializationTrait;

  /**
   * Media settings map.
   *
   * @var array
   */
  protected $mediaSettingsMap;

  /**
   * The file handler.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The http client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a MediaGenerator object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \GuzzleHttp\Client $http_client
   *   The http client service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(FileSystemInterface $file_system, EntityTypeManagerInterface $entity_type_manager, Token $token, AccountProxyInterface $current_user, FileRepositoryInterface $file_repository, LoggerInterface $logger, Client $http_client) {
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
    $this->token = $token;
    $this->currentUser = $current_user;
    $this->fileRepository = $file_repository;
    $this->logger = $logger;
    $this->httpClient = $http_client;
    $this->setMediaSettingsMap();
  }

  /**
   * Gets the media settings map.
   *
   * @return array
   *   The settings map.
   */
  public function getMediaSettingsMap() {
    return $this->mediaSettingsMap;
  }

  /**
   * Generates media entity from remote file.
   *
   * @param string $remote_file_path
   *   The remote file URL.
   * @param array $paths_overrides
   *   The paths overrides for media types.
   *
   * @return \Drupal\media\MediaInterface|null
   *   The media entity if created or NULL.
   *
   * @throws \Drupal\Core\File\Exception\FileException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \InvalidArgumentException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function generateFromRemoteFile($remote_file_path, array $paths_overrides = []) {
    $generated_media = NULL;
    $ext = pathinfo($remote_file_path, PATHINFO_EXTENSION);
    // Check if file extension.
    if (strlen($ext) >= 3 && strlen($ext) <= 4) {
      $media_type_settings = $this->getMediaTypeSettingsFromExt($ext);
      if ($media_type_settings) {
        $media_type_id = $media_type_settings['id'];
        $dest_dir = isset($paths_overrides[$media_type_id]) && $paths_overrides[$media_type_id] ? $paths_overrides[$media_type_id] : $media_type_settings['file_directory'];
        // Replace tokens.
        $dest_dir = $media_type_settings['uri_scheme'] . '://' . $this->token->replace($dest_dir);
        if ($this->fileSystem->prepareDirectory($dest_dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
          $parsed_file = parse_url($remote_file_path);
          $info = pathinfo($parsed_file['path']);
          $file_name = $info['basename'];
          $existing = $this->getMediaByName($file_name);
          if ($existing) {
            return $existing;
          }
          else {
            $file_name = $this->fileSystem->createFilename($file_name, $dest_dir);
            if ($file = $this->retrieveFile($remote_file_path, $file_name, FileSystemInterface::EXISTS_RENAME)) {
              $media = Media::create([
                'name' => $info['basename'],
                'bundle' => $media_type_id,
                'uid' => $this->currentUser->id(),
              ]);
              /** @var \Drupal\media\MediaTypeInterface $media_type */
              $media_type = $media->bundle->entity;
              $source_field = $media->getSource()->getSourceFieldDefinition($media_type)->getName();
              $media->set($source_field, [
                'target_id' => $file->id(),
              ]);
              $media->save();
              $generated_media = $media;
            }
          }
        }
      }
    }
    return $generated_media;
  }

  /**
   * Sets the media type source settings map.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function setMediaSettingsMap() {
    $types = $this->entityTypeManager
      ->getStorage('media_type')
      ->loadMultiple();
    /** @var \Drupal\media\Entity\MediaType $media_type */
    foreach ($types as $media_type_id => $media_type) {
      $source_field = $media_type->getSource()->getSourceFieldDefinition($media_type);
      $source_field_settings = $source_field->getSettings();
      $this->mediaSettingsMap[$media_type_id] = $source_field_settings;
      $this->mediaSettingsMap[$media_type_id]['label'] = $media_type->label();
    }
  }

  /**
   * Sets the media type source settings map.
   *
   * @param string $extension
   *   The extension.
   *
   * @return array|bool
   *   The settings or FALSE if extension not found.
   */
  protected function getMediaTypeSettingsFromExt($extension) {
    foreach ($this->mediaSettingsMap as $media_type_id => $settings) {
      if (isset($settings['file_extensions']) && strpos($settings['file_extensions'], $extension) !== FALSE) {
        $settings['id'] = $media_type_id;
        return $settings;
      }
    }
    return FALSE;
  }

  /**
   * Gets a media entity by name.
   *
   * @param string $file_name
   *   The media name.
   *
   * @return \Drupal\media\MediaInterface|null
   *   The media entity if found or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getMediaByName($file_name) {
    $media_storage = $this->entityTypeManager->getStorage('media');
    $existing_by_name = $media_storage->loadByProperties([
      'name' => $file_name,
    ]);
    if ($existing_by_name) {
      return reset($existing_by_name);
    }
    return NULL;
  }

  /**
   * Attempts to get a file using Guzzle HTTP client and to store it locally.
   *
   * @param string $url
   *   The URL of the file to grab.
   * @param string $destination
   *   Stream wrapper URI specifying where the file should be placed. If a
   *   directory path is provided, the file is saved into that directory under
   *   its original name. If the path contains a filename as well, that one will
   *   be used instead.
   *   If this value is omitted, the site's default files scheme will be used,
   *   usually "public://".
   * @param int $replace
   *   Replace behavior when the destination file already exists:
   *   - FileSystemInterface::EXISTS_REPLACE: Replace the existing file.
   *   - FileSystemInterface::EXISTS_RENAME: Append _{incrementing number} until
   *     the filename is unique.
   *   - FileSystemInterface::EXISTS_ERROR: Do nothing and return FALSE.
   *
   * @return mixed
   *   One of these possibilities:
   *   - If it succeeds a \Drupal\file\FileInterface
   *     object which describes the file.
   *   - If it fails, FALSE.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function retrieveFile($url, $destination, $replace = FileSystemInterface::EXISTS_RENAME) {
    $parsed_url = parse_url($url);
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = $this->fileSystem;
    if (is_dir($file_system->realpath($destination))) {
      // Prevent URIs with triple slashes when glueing parts together.
      $path = str_replace('///', '//', "$destination/") . $file_system->basename($parsed_url['path']);
    }
    else {
      $path = $destination;
    }
    try {
      $data = (string) $this->httpClient
        ->get($url, ['verify' => FALSE])
        ->getBody();
      $local = $this->fileRepository->writeData($data, $path, $replace);
    }
    catch (TransferException $exception) {
      $this->logger->error('Failed to fetch file due to error "%error"', ['%error' => $exception->getMessage()]);
      return FALSE;
    }
    catch (FileException | InvalidStreamWrapperException $e) {
      $this->logger->error('Failed to save file due to error "%error"', ['%error' => $e->getMessage()]);
      return FALSE;
    }
    if (!$local) {
      $this->logger->error('@remote could not be saved to @path.', [
        '@remote' => $url,
        '@path' => $path,
      ]);
    }

    return $local;
  }

}
