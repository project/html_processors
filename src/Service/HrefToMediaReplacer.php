<?php

namespace Drupal\html_processors\Service;

use Drupal\html_processors\Service\MediaGenerator;

/**
 * Service to parse HTML to Gutenberg.
 */
class HrefToMediaReplacer {

  /**
   * The media generator service.
   *
   * @var \Drupal\html_processors\Service\MediaGenerator
   */
  protected $mediaGenerator;

  /**
   * Constructs a HrefToMediaReplacer object.
   *
   * @param \Drupal\html_processors\Service\MediaGenerator $media_generator
   *   The media generator service.
   */
  public function __construct(MediaGenerator $media_generator) {
    $this->mediaGenerator = $media_generator;
  }

  /**
   * Process HTML to generate media items and replace href attributes.
   *
   * @param string $data
   *   The original HTML.
   * @param array $config
   *   The config to process media items.
   *
   * @return string
   *   The processed HTML.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\File\Exception\FileException
   * @throws \InvalidArgumentException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function process($data, array $config) {
    $remote_url = $config['remote_url'];
    $only_relative = $config['only_relative'];
    // Load the HTML as a DOM document to traverse through.
    $source = new \DOMDocument('5.0', 'UTF-8');
    try {
      @$source->loadHTML(mb_convert_encoding($data, 'HTML-ENTITIES', 'UTF-8'));
    }
    catch (\Exception $th) {
      // Skipping malformed HTML.
    }
    $source->encoding = 'utf-8';

    $links = $source->getElementsByTagName('a');
    // Iterate each link.
    foreach ($links as $link) {
      $href = $link->getAttribute('href');
      if ($href) {
        $remote_file_path = $href;
        // Skip when only relative is enable.
        if ($only_relative && str_starts_with($href, 'http')) {
          continue;
        }
        // Prepend source site URL if relative path.
        if (!str_starts_with($href, 'http')) {
          $remote_file_path = $remote_url . $href;
        }
        /** @var \Drupal\media\MediaInterface $media */
        $media = $this->mediaGenerator->generateFromRemoteFile($remote_file_path, $config);
        if ($media) {
          /** @var \Drupal\media\MediaTypeInterface $media_type */
          $media_type = $media->bundle->entity;
          $source_field = $media->getSource()->getSourceFieldDefinition($media_type)->getName();
          /** @var \Drupal\file\FileInterface $file */
          $file = $media->get($source_field)->entity;
          // Updating link href.
          $link->setAttribute('href', $file->createFileUrl());
        }
      }
    }
    $result = preg_replace('/<!DOCTYPE .*>/', '', $source->saveHTML());
    $result = preg_replace('/<html><body>/', '', $result);
    $result = preg_replace('/<\/body><\/html>/', '', $result);
    return $result;
  }

}
