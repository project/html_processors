<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;

/**
 * Plugin implementation of the html_gutenberg_processor for maps.
 *
 * @HtmlGutenbergProcessor(
 *   id = "map",
 *   label = @Translation("Map"),
 *   tag = "map",
 *   comment = "wp:html",
 *   description = @Translation("Wraps <map> tags as html and move them to root."),
 *   weight = 8
 * )
 */
class Map extends HtmlGutenbergProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function process(\DOMDocument &$source) {
    $tag = $this->pluginDefinition['tag'];
    $comment = $this->pluginDefinition['comment'];
    $classes = $this->pluginDefinition['classes'] ?? [];
    $elements = $source->getElementsByTagName($tag);
    // Iterate each element.
    foreach ($elements as $element) {
      if (!$element->getAttribute('name')) {
        return;
      }
      // Move to root and set name.
      $element->setAttribute('name', 'Map');
      $this->moveToRoot($element);
      // Start comment.
      $start_comment = $source->createComment(" $comment ");
      $element->parentNode->insertBefore($start_comment, $element);
      // End comment.
      // Strip out options like {"level":3} from the tag to build the end tag.
      preg_match("/(.+)\s/", $comment, $matches);
      $end_comment_tag = "/" . ($matches[1] ?? $comment);
      $end_comment = $source->createComment(" $end_comment_tag ");
      $element->parentNode->insertBefore($end_comment, $element->nextSibling);
      // Add classes if they were provided.
      if ($classes) {
        $classes_string = implode(' ', $classes);
        $element->setAttribute("class", $classes_string);
      }
    }
  }

}
