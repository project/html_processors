<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the html_gutenberg_processor for images.
 *
 * @HtmlGutenbergProcessor(
 *   id = "table",
 *   label = @Translation("Table"),
 *   tag = "table",
 *   comment = "wp:table",
 *   description = @Translation("Add <table> tag comments."),
 *   weight = 70
 * )
 */
class Table extends HtmlGutenbergProcessorBase {

  const FIRST_ROW_TO_THEAD = 'first_row_to_thead';
  const HEADER_ATTR = 'header_attribute';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config[self::FIRST_ROW_TO_THEAD] = FALSE;
    $config[self::HEADER_ATTR] = '';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form[self::FIRST_ROW_TO_THEAD] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Convert first row to header'),
      '#default_value' => $this->getSetting(self::FIRST_ROW_TO_THEAD),
    ];
    $form[self::HEADER_ATTR] = [
      '#type' => 'textfield',
      '#title' => $this->t('Conditional header attribute'),
      '#default_value' => $this->getSetting(self::HEADER_ATTR),
      '#description' => $this->t('If set the first row will be converted to header if the td has the attribute'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->setConfiguration([
      self::FIRST_ROW_TO_THEAD => $form_state->getValue(self::FIRST_ROW_TO_THEAD),
      self::HEADER_ATTR => $form_state->getValue(self::HEADER_ATTR),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \DOMException
   */
  public function process(\DOMDocument &$source) {
    $tag = $this->pluginDefinition['tag'];
    $comment = $this->pluginDefinition['comment'];
    $classes = $this->pluginDefinition['classes'] ?? [];
    $convert_first_row = $this->configuration[self::FIRST_ROW_TO_THEAD];
    $conditional_attribute = $this->configuration[self::HEADER_ATTR];
    $elements = $source->getElementsByTagName($tag);
    // Iterate each element.
    foreach ($elements as $element) {
      // Remove table attributes.
      $attributes = $element->attributes;
      while ($attributes->length) {
        /** @var \DOMAttr $first_attr */
        $first_attr = $attributes->item(0);
        $element->removeAttribute($first_attr->name);
      }
      // Add tbody.
      $tbody = $source->createElement('tbody');
      $element->insertBefore($tbody, $element->firstChild);
      // Iterate rows.
      $rows = $element->getElementsByTagName('tr');
      $row_index = 1;
      foreach ($rows as $row) {
        // Move row to thead or tbody.
        $create_header = FALSE;
        if ($convert_first_row && $row_index === 1) {
          $create_header = TRUE;
          // Conditional attribute.
          if ($conditional_attribute && !$row->hasAttribute($conditional_attribute)) {
            $create_header = FALSE;
          }
        }
        if ($create_header) {
          $thead = $source->createElement('thead');
          $thead->appendChild($row);
          $element->insertBefore($thead, $element->firstChild);
        }
        else {
          $tbody->appendChild($row);
        }
        // Remove row attributes.
        $this->removeAttributes($row);
        $row_index++;
      }

      // Start comment.
      $start_comment = $source->createComment(" $comment ");
      $element->parentNode->insertBefore($start_comment, $element);
      // End comment.
      // Strip out options like {"level":3} from the tag to build the end tag.
      preg_match("/(.+)\s/", $comment, $matches);
      $end_comment_tag = "/" . ($matches[1] ?? $comment);
      $end_comment = $source->createComment(" $end_comment_tag ");
      $element->parentNode->insertBefore($end_comment, $element->nextSibling);
      $element->normalize();
      // Create wrapper figure.
      $figure_wrapper = $source->createElement('figure');
      $figure_wrapper->setAttribute('class', 'wp-block-table');
      $element->parentNode->replaceChild($figure_wrapper, $element);
      $figure_wrapper->appendChild($element);
      // Add classes if they were provided.
      if ($classes) {
        $classes_string = implode(' ', $classes);
        $element->setAttribute("class", $classes_string);
      }
    }
  }

  /**
   * Remove element attributes.
   *
   * @param \DOMElement $row
   *   The element.
   */
  protected function removeAttributes(\DOMElement $row) {
    $attributes = $this->getAttributesNames($row);
    foreach ($attributes as $attribute) {
      $row->removeAttribute($attribute);
    }
  }

  /**
   * Return element attributes names.
   *
   * @param \DOMElement $row
   *   The element.
   */
  protected function getAttributesNames(\DOMElement $row) {
    $attributes = $row->attributes;
    for ($i = 0; $i < $attributes->length; $i++) {
      /** @var \DOMAttr $attr */
      $attr = $attributes->item($i);
      yield $attr->name;
    }
  }

}
