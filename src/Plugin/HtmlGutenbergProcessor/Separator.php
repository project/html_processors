<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;

/**
 * Plugin implementation of the html_gutenberg_processor for separators.
 *
 * @HtmlGutenbergProcessor(
 *   id = "separator",
 *   label = @Translation("Separator"),
 *   tag = "hr",
 *   comment = "wp:separator",
 *   description = @Translation("Add <hr> tag comments."),
 *   classes = {
 *     "wp-block-separator",
 *     "has-alpha-channel-opacity",
 *   },
 *   weight = 60
 * )
 */
class Separator extends HtmlGutenbergProcessorBase {

}
