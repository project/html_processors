<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;

/**
 * Plugin implementation of the html_gutenberg_processor for ordered lists.
 *
 * @HtmlGutenbergProcessor(
 *   id = "ordered_list",
 *   label = @Translation("Ordered List"),
 *   tag = "ol",
 *   comment = "wp:list {""ordered"":true}",
 *   description = @Translation("Add <ol> tag comments."),
 *   weight = 30
 * )
 */
class OrderedList extends HtmlGutenbergProcessorBase {

}
