<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;

/**
 * Plugin implementation of the html_gutenberg_processor for paragraphs.
 *
 * @HtmlGutenbergProcessor(
 *   id = "paragraph",
 *   label = @Translation("Paragraph"),
 *   tag = "p",
 *   comment = "wp:paragraph",
 *   description = @Translation("Add <p> tag comments."),
 *   weight = 80
 * )
 */
class Paragraph extends HtmlGutenbergProcessorBase {

  /**
   * {@inheritdoc}
   *
   * @throws \DOMException
   */
  public function process(\DOMDocument &$source) {
    $this->processStrong($source);
    $this->processCenter($source);
    // Default p tag process.
    parent::process($source);
  }

  /**
   * Process center tags.
   *
   * @param \DOMDocument $source
   *   The source HTML.
   *
   * @throws \DOMException
   */
  protected function processCenter(\DOMDocument &$source) {
    $tag = 'center';
    $elements = $source->getElementsByTagName($tag);
    // Iterate each element.
    foreach ($elements as $element) {
      /** @var \DOMElement $parent */
      $parent = $element->parentNode;
      if ($parent) {
        switch ($parent->nodeName) {
          case 'p':
            $this->unwrap($element);
            $parent->setAttribute('class', $parent->getAttribute('class') . ' has-text-align-center');
            break;

          case 'body':
            // Wrap in paragraph.
            $paragraph_wrapper = $source->createElement('p');
            $paragraph_wrapper->setAttribute('class', 'has-text-align-center');
            $element->parentNode->replaceChild($paragraph_wrapper, $element);
            $paragraph_wrapper->appendChild($element);
            // Remove center tag.
            $this->unwrap($element);
            break;
        }
      }
    }
  }

  /**
   * Process strong tags.
   *
   * @param \DOMDocument $source
   *   The source HTML.
   *
   * @throws \DOMException
   */
  protected function processStrong(\DOMDocument &$source) {
    $tag = 'strong';
    $elements = $source->getElementsByTagName($tag);
    // Iterate each element.
    foreach ($elements as $element) {
      if ($element->parentNode && $element->parentNode->nodeName == 'body') {
        // Wrap in paragraph.
        $paragraph_wrapper = $source->createElement('p');
        $element->parentNode->replaceChild($paragraph_wrapper, $element);
        $paragraph_wrapper->appendChild($element);
      }
    }
  }

}
