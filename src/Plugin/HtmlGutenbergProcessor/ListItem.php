<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;

/**
 * Plugin implementation of the html_gutenberg_processor for list items.
 *
 * @HtmlGutenbergProcessor(
 *   id = "list_item",
 *   label = @Translation("List Items"),
 *   tag = "li",
 *   comment = "wp:list-item",
 *   description = @Translation("Add <li> tag comments."),
 *   weight = 50
 * )
 */
class ListItem extends HtmlGutenbergProcessorBase {

}
