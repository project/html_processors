<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\html_processors\Service\MediaGenerator;
use Drupal\Component\Serialization\Json;

/**
 * Plugin implementation of the html_gutenberg_processor for images.
 *
 * @HtmlGutenbergProcessor(
 *   id = "image",
 *   label = @Translation("Image"),
 *   tag = "img",
 *   comment = "wp:drupalmedia/drupal-media-entity",
 *   description = @Translation("Replace <img> tag for drupal media."),
 *   weight = 10
 * )
 */
class Image extends HtmlGutenbergProcessorBase implements ContainerFactoryPluginInterface {

  const REMOTE_SITE_URL = 'remote_url';

  /**
   * The media generator service.
   *
   * @var \Drupal\html_processors\Service\MediaGenerator
   */
  protected $mediaGenerator;

  /**
   * Constructs a new HrefToMedia instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\html_processors\Service\MediaGenerator $media_generator
   *   The media generator service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MediaGenerator $media_generator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mediaGenerator = $media_generator;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('html_processors.media_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config[self::REMOTE_SITE_URL] = '';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    // Remote site URL.
    $form[self::REMOTE_SITE_URL] = [
      '#type' => 'url',
      '#title' => $this->t('Remote site URL'),
      '#default_value' => $this->getSetting(self::REMOTE_SITE_URL),
      '#description' => $this->t('The remote site URL without trailing "/"'),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Ensure no trailing "/".
    $remote_url = $form_state->getValue(self::REMOTE_SITE_URL);
    $last_char = substr($remote_url, -1);
    if ($last_char === "/") {
      $form_state->setError($form[self::REMOTE_SITE_URL], $this->t('The remote site URL should not have a trailing "/"'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->setConfiguration([
      self::REMOTE_SITE_URL => $form_state->getValue(self::REMOTE_SITE_URL),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\File\Exception\FileException
   * @throws \InvalidArgumentException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \DOMException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function process(\DOMDocument &$source) {
    $tag = $this->pluginDefinition['tag'];
    $comment = $this->pluginDefinition['comment'];
    $remote_url = $this->configuration[self::REMOTE_SITE_URL] ?? '';
    // Not possible to process if no remote url provided.
    if (!$remote_url) {
      return;
    }
    $elements = $source->getElementsByTagName($tag);
    // Iterate each element.
    foreach ($elements as $element) {
      $src = $element->getAttribute('src');
      if ($src) {
        $remote_file_path = $this->prepareImageUrl($src, $remote_url);
        /** @var \Drupal\media\MediaInterface $media */
        $media = $this->mediaGenerator->generateFromRemoteFile($remote_file_path);
        if ($media) {
          $component_type = 'media';
          $parent_type = $element->parentNode->nodeName;
          if ($parent_type === 'a') {
            $component_type = 'image';
            $parent_type = $element->parentNode->parentNode->nodeName;
          }
          if ($parent_type === 'td') {
            $component_type = 'inline_image';
          }
          // Generate component.
          switch ($component_type) {
            case 'media':
              $this->moveToRoot($element);
              // Prepare attributes.
              $component_attrs = [
                'mediaEntityIds' => [$media->id()],
              ];
              // Add map view mode if usemap attribute present.
              if ($element->getAttribute('usemap')) {
                $component_attrs['viewMode'] = 'map';
              }
              // Add align.
              if ($align = $element->getAttribute('align')) {
                $component_attrs['align'] = $align;
              }
              $comment = $source->createComment(' wp:drupalmedia/drupal-media-entity ' . Json::encode($component_attrs) . ' /');
              $element->parentNode->replaceChild($comment, $element);
              break;

            case 'inline_image':
              /** @var \Drupal\media\MediaTypeInterface $media_type */
              $media_type = $media->bundle->entity;
              $source_field = $media->getSource()->getSourceFieldDefinition($media_type)->getName();
              /** @var \Drupal\file\FileInterface $file */
              $file = $media->get($source_field)->entity;
              // Updating img scr.
              $element->setAttribute('src', $file->createFileUrl());
              break;

            case 'image':
              /** @var \Drupal\media\MediaTypeInterface $media_type */
              $media_type = $media->bundle->entity;
              $source_field = $media->getSource()->getSourceFieldDefinition($media_type)->getName();
              /** @var \Drupal\file\FileInterface $file */
              $file = $media->get($source_field)->entity;
              // Add image attributes.
              $element->setAttribute('data-entity-type', 'file');
              $element->setAttribute('data-entity-uuid', $file->uuid());
              $element->setAttribute('data-image-style', 'original');
              $element->removeAttribute('width');
              // Updating img scr.
              $element->setAttribute('src', $file->createFileUrl());
              // Create wrapper figure.
              $parent_link = $element->parentNode;
              $this->moveToRoot($parent_link);
              $figure_wrapper = $source->createElement('figure');
              $figure_wrapper->setAttribute('class', 'wp-block-image size-full');
              $parent_link->parentNode->replaceChild($figure_wrapper, $parent_link);
              $figure_wrapper->appendChild($parent_link);
              // Add comment.
              $media_attrs = [
                'data-entity-type' => 'file',
                'data-entity-uuid' => $file->uuid(),
                'data-image-style' => 'original',
              ];
              $component_attrs = [
                'id' => $file->id(),
                'sizeSlug' => 'full',
                'linkDestination' => 'custom',
                'mediaAttrs' => $media_attrs,
              ];
              // Start comment.
              $start_comment = $source->createComment(' wp:image ' . Json::encode($component_attrs) . ' ');
              $figure_wrapper->parentNode->insertBefore($start_comment, $figure_wrapper);
              // End comment.
              $end_comment = $source->createComment(" /wp:image ");
              $figure_wrapper->parentNode->insertBefore($end_comment, $figure_wrapper->nextSibling);
              break;
          }
        }
      }
    }
  }

  /**
   * Prepare the remote image URL.
   *
   * @param string $remote_file_path
   *   The path to the file.
   * @param string $remote_url
   *   The remote site URL.
   *
   * @return string
   *   The prepared URL.
   */
  protected function prepareImageUrl($remote_file_path, $remote_url) {
    // Improved bad formatter url.
    $remote_file_path = str_replace('\\', '/', $remote_file_path);
    if (!str_starts_with($remote_file_path, 'http')) {
      // Force relative path to remote site root.
      if (str_starts_with($remote_file_path, '../')) {
        $remote_file_path = '/' . str_replace('../', '', $remote_file_path);
      }
      $remote_file_path = $remote_url . $remote_file_path;
    }
    return $remote_file_path;
  }

}
