<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;

/**
 * Plugin implementation of the html_gutenberg_processor for containers.
 *
 * @HtmlGutenbergProcessor(
 *   id = "container",
 *   label = @Translation("Container"),
 *   tag = "div",
 *   comment = "wp:html",
 *   description = @Translation("Add <div> tag comments only at first level."),
 *   weight = 100
 * )
 */
class Container extends HtmlGutenbergProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function process(\DOMDocument &$source) {
    $tag = $this->pluginDefinition['tag'];
    $comment = $this->pluginDefinition['comment'];
    $classes = $this->pluginDefinition['classes'] ?? [];
    $body = $source->getElementsByTagName('body')[0];
    // Iterate body element.
    // Only first level divs need the comment.
    foreach ($body->childNodes as $element) {
      if ($element->nodeName === $tag) {
        // Start comment.
        $start_comment = $source->createComment(" $comment ");
        $element->parentNode->insertBefore($start_comment, $element);
        // End comment.
        // Strip out options like {"level":3} from the tag to build the end tag.
        preg_match("/(.+)\s/", $comment, $matches);
        $end_comment_tag = "/" . ($matches[1] ?? $comment);
        $end_comment = $source->createComment(" $end_comment_tag ");
        $element->parentNode->insertBefore($end_comment, $element->nextSibling);
        // Add classes if they were provided.
        if ($classes) {
          $classes_string = implode(' ', $classes);
          $element->setAttribute("class", $classes_string);
        }
      }
    }
  }

}
