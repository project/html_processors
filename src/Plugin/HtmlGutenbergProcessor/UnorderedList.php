<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;

/**
 * Plugin implementation of the html_gutenberg_processor for unordered lists.
 *
 * @HtmlGutenbergProcessor(
 *   id = "unordered_list",
 *   label = @Translation("Unordered List"),
 *   tag = "ul",
 *   comment = "wp:list",
 *   description = @Translation("Add <ul> tag comments."),
 *   weight = 40
 * )
 */
class UnorderedList extends HtmlGutenbergProcessorBase {

}
