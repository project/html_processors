<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;

/**
 * Plugin implementation of the html_gutenberg_processor for headings.
 *
 * @HtmlGutenbergProcessor(
 *   id = "heading",
 *   label = @Translation("Heading"),
 *   tag = "h",
 *   comment = "wp:heading",
 *   description = @Translation("Add <h*> tag comments."),
 *   weight = 20
 * )
 */
class Heading extends HtmlGutenbergProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function process(\DOMDocument &$source) {
    $tag = $this->pluginDefinition['tag'];
    $comment = $this->pluginDefinition['comment'];
    for ($level = 1; $level <= 6; $level++) {
      $elements = $source->getElementsByTagName($tag . $level);
      // Iterate each link.
      foreach ($elements as $element) {
        // Start comment.
        $start_comment = $source->createComment(' ' . $comment . ' {"level":' . $level . '} ');
        $element->parentNode->insertBefore($start_comment, $element);
        // End comment.
        $end_comment = $source->createComment(" /$comment ");
        $element->parentNode->insertBefore($end_comment, $element->nextSibling);
      }
    }
  }

}
