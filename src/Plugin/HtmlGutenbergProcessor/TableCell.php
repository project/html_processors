<?php

namespace Drupal\html_processors\Plugin\HtmlGutenbergProcessor;

use Drupal\html_processors\HtmlGutenbergProcessorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the html_gutenberg_processor for table cells.
 *
 * @HtmlGutenbergProcessor(
 *   id = "table_cell",
 *   label = @Translation("Table Cell"),
 *   tag = "td",
 *   comment = "wp:table",
 *   description = @Translation("Clean table cells."),
 *   weight = 9
 * )
 */
class TableCell extends HtmlGutenbergProcessorBase {

  const ALLOWED_NODE_NAMES = 'allowed_node_names';
  const ALLOWED_ATTRS = 'allowed_attributes';

  /**
   * The allowed node types.
   *
   * @var array
   */
  protected $allowedNodeTypes;

  /**
   * The allowed attributes.
   *
   * @var array
   */
  protected $allowedAttributes;

  /**
   * Constructs a new HrefToMedia instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setAllowedNodeTypes();
    $this->setAllowedAttributes();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config[self::ALLOWED_NODE_NAMES] = 'a #text strong img br';
    $config[self::ALLOWED_ATTRS] = 'class';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form[self::ALLOWED_NODE_NAMES] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed node names'),
      '#default_value' => $this->getSetting(self::ALLOWED_NODE_NAMES),
      '#description' => $this->t('Other elements than these will be removed from the cell content.'),
      '#required' => TRUE,
    ];
    $form[self::ALLOWED_ATTRS] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed attributes'),
      '#default_value' => $this->getSetting(self::ALLOWED_ATTRS),
      '#description' => $this->t('Other attributes than these will be removed from the cell.'),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->setConfiguration([
      self::ALLOWED_NODE_NAMES => $form_state->getValue(self::ALLOWED_NODE_NAMES),
      self::ALLOWED_ATTRS => $form_state->getValue(self::ALLOWED_ATTRS),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function process(\DOMDocument &$source) {
    $tag = $this->pluginDefinition['tag'];
    $cells = $source->getElementsByTagName($tag);
    // Iterate each element.
    foreach ($cells as $cell) {
      // Remove attributes.
      $attributes = $cell->attributes;
      $attrs_to_delete = [];
      for ($i = 0; $i < $attributes->length; $i++) {
        /** @var \DOMAttr $attr */
        $attr = $attributes->item($i);
        if (!in_array($attr->name, $this->allowedAttributes)) {
          $attrs_to_delete[] = $attr->name;
        }
      }
      if ($attrs_to_delete) {
        foreach ($attrs_to_delete as $attr) {
          $cell->removeAttribute($attr);
        }
      }
      // Remove not allowed children.
      $this->cleanCell($cell, $source);
    }
  }

  /**
   * Set the allowed node types inside cells.
   */
  protected function setAllowedNodeTypes() {
    if (empty($this->allowedNodeTypes)) {
      if ($this->configuration[self::ALLOWED_NODE_NAMES]) {
        $this->allowedNodeTypes = explode(' ', $this->configuration[self::ALLOWED_NODE_NAMES]);
      }
    }
  }

  /**
   * Set the allowed node types inside cells.
   */
  protected function setAllowedAttributes() {
    if (empty($this->allowedAttributes)) {
      if ($this->configuration[self::ALLOWED_ATTRS]) {
        $this->allowedAttributes = explode(' ', $this->configuration[self::ALLOWED_ATTRS]);
      }
    }
  }

  /**
   * Clean the cell to leave only allowed children.
   *
   * @param \DOMElement $cell
   *   The cell to clean.
   * @param \DOMDocument $source
   *   The source HTML.
   */
  protected function cleanCell(\DOMElement $cell, \DOMDocument &$source) {
    $recheck = FALSE;
    foreach ($cell->childNodes as $element) {
      if (!in_array($element->nodeName, $this->allowedNodeTypes)) {
        if ($element->nodeName === 'center') {
          $cell->setAttribute('class', $cell->getAttribute('class') . ' has-text-align-center');
          $cell->setAttribute('data-align', 'center');
        }
        $this->unwrap($element);
        $recheck = TRUE;
      }
      else {
        if ($element->nodeName == 'strong') {
          $element->parentNode->replaceChild($source->createTextNode($element->textContent), $element);
        }
      }
    }
    if ($recheck) {
      $this->cleanCell($cell, $source);
    }
  }

}
