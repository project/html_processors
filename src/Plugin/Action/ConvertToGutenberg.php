<?php

namespace Drupal\html_processors\Plugin\Action;

use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\html_processors\HtmlGutenbergProcessorConfigTrait;
use Drupal\html_processors\Service\HtmlGutenbergParser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a Convert body HTML to Gutenberg action.
 *
 * @Action(
 *   id = "html_processors_convert_to_gutenberg",
 *   label = @Translation("Convert body HTML to Gutenberg"),
 *   type = "node",
 *   category = @Translation("HtmlProcessors")
 * )
 */
class ConvertToGutenberg extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  use HtmlGutenbergProcessorConfigTrait;

  /**
   * The HTML to Gutenberg parser.
   *
   * @var \Drupal\html_processors\Service\HtmlGutenbergParser
   */
  protected $htmlGutenbergParser;

  /**
   * Constructs a new SocialLinksBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\html_processors\Service\HtmlGutenbergParser $html_gutenberg_parser
   *   The HTML to Gutenberg parser.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, HtmlGutenbergParser $html_gutenberg_parser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->htmlGutenbergParser = $html_gutenberg_parser;
    $this->setHtmlGutenbergProcessors();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('html_processors.html_gutenberg_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function access($node, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\node\NodeInterface $node */
    $access = $node->access('update', $account, TRUE)
      ->andIf($node->title->access('edit', $account, TRUE));
    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \InvalidArgumentException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function execute($node = NULL) {
    /** @var \Drupal\node\NodeInterface $node */
    $body = $node->body->value;
    $processors_config = $this->configuration['gutenberg_processors'];
    $processed_body = $this->htmlGutenbergParser->parse($body, $processors_config);
    $node->set('body', $processed_body)->save();
  }

}
