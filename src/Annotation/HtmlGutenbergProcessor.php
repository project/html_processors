<?php

namespace Drupal\html_processors\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines html_gutenberg_processor annotation object.
 *
 * @Annotation
 */
class HtmlGutenbergProcessor extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin weight.
   *
   * @var int
   */
  public $weight;

  /**
   * The plugin tag.
   *
   * @var string
   */
  public $tag;

  /**
   * The comment for Gutenberg.
   *
   * @var string
   */
  public $comment;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The plugin classes.
   *
   * @var array
   */
  public $classes;

}
