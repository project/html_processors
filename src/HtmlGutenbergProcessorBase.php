<?php

namespace Drupal\html_processors;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for html_gutenberg_processor plugins.
 */
abstract class HtmlGutenbergProcessorBase extends PluginBase implements HtmlGutenbergProcessorInterface {

  use StringTranslationTrait;

  const DISABLED = 'disabled';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function process(\DOMDocument &$source) {
    $tag = $this->pluginDefinition['tag'];
    $comment = $this->pluginDefinition['comment'];
    $classes = $this->pluginDefinition['classes'] ?? [];
    $elements = $source->getElementsByTagName($tag);
    // Iterate each element.
    foreach ($elements as $element) {
      // Start comment.
      $start_comment = $source->createComment(" $comment ");
      $element->parentNode->insertBefore($start_comment, $element);
      // End comment.
      // Strip out options like {"level":3} from the tag to build the end tag.
      preg_match("/(.+)\s/", $comment, $matches);
      $end_comment_tag = "/" . ($matches[1] ?? $comment);
      $end_comment = $source->createComment(" $end_comment_tag ");
      $element->parentNode->insertBefore($end_comment, $element->nextSibling);
      // Add classes if they were provided.
      if ($classes) {
        $classes_string = implode(' ', $classes);
        $element->setAttribute("class", $classes_string);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config[self::DISABLED] = FALSE;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form[self::DISABLED] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable this processor'),
      '#default_value' => $this->getSetting(self::DISABLED),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration([
      self::DISABLED => $form_state->getValue(self::DISABLED),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($key) {
    return $this->configuration[$key] ?? NULL;
  }

  /**
   * Extract all children of an element.
   *
   * @param \DOMNode $el
   *   The element to unwrap.
   */
  protected function unwrap(\DOMNode $el) {
    if ($el->parentNode) {
      // Move all children out of the element.
      while ($el->firstChild) {
        $el->parentNode->insertBefore($el->firstChild, $el);
      }
      // Remove the empty element.
      $el->parentNode->removeChild($el);
    }
  }

  /**
   * Move an element up to the hierarchy until the body/root.
   *
   * @param \DOMNode $el
   *   The element to unwrap.
   */
  protected function moveToRoot(\DOMNode $el) {
    if ($el->parentNode && !($el->parentNode->nodeName == 'body' || $el->parentNode->nodeName == '#document')) {
      $parent = $el->parentNode;
      $parent->parentNode->insertBefore($el, $parent);
      if (!$parent->hasChildNodes()) {
        $parent->parentNode->removeChild($parent);
      }
      $this->moveToRoot($el);
    }
  }

}
