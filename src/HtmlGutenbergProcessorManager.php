<?php

namespace Drupal\html_processors;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * HtmlGutenbergProcessor plugin manager.
 */
class HtmlGutenbergProcessorManager extends DefaultPluginManager {

  /**
   * Constructs HtmlGutenbergProcessorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/HtmlGutenbergProcessor',
      $namespaces,
      $module_handler,
      'Drupal\html_processors\HtmlGutenbergProcessorInterface',
      'Drupal\html_processors\Annotation\HtmlGutenbergProcessor'
    );
    $this->alterInfo('html_gutenberg_processor_info');
    $this->setCacheBackend($cache_backend, 'html_gutenberg_processor_plugins');
  }

  /**
   * Gets the definition of all plugins for this type sorted by weight.
   *
   * @return array
   *   The definitions.
   */
  public function getSortedDefinitions() {
    $definitions = $this->getDefinitions();
    uasort($definitions, function ($a, $b) {
      if ($a['weight'] == $b['weight']) {
        return 0;
      }
      return ($a['weight'] < $b['weight']) ? -1 : 1;
    });
    return $definitions;
  }

}
