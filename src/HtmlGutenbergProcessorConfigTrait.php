<?php

namespace Drupal\html_processors;

use Drupal\Core\Form\SubformState;
use Drupal\Core\Form\FormStateInterface;

/**
 * Wrapper methods for HTML to Gutenberg processors configuration.
 */
trait HtmlGutenbergProcessorConfigTrait {

  /**
   * The HTML to Gutenberg processors.
   *
   * @var \Drupal\html_processors\HtmlGutenbergProcessorInterface[]
   */
  protected $htmlGutenbergProcessors;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    foreach ($this->htmlGutenbergProcessors as $plugin_id => $processor_plugin) {
      $form[$plugin_id] = [
        '#type' => 'details',
        '#title' => $processor_plugin->label(),
        '#tree' => TRUE,
      ];
      $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
      $form[$plugin_id] = $processor_plugin->buildConfigurationForm($form[$plugin_id], $subform_state);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->htmlGutenbergProcessors as $plugin_id => $processor_plugin) {
      // Ensure plugin has config form.
      if (isset($form[$plugin_id])) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $processor_plugin->validateConfigurationForm($form[$plugin_id], $subform_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $processors_config = [];
    foreach ($this->htmlGutenbergProcessors as $plugin_id => $processor_plugin) {
      // Ensure plugin has config form.
      if (isset($form[$plugin_id])) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $processor_plugin->submitConfigurationForm($form[$plugin_id], $subform_state);
        $processors_config[$plugin_id] = $processor_plugin->getConfiguration();
        $processors_config[$plugin_id]['id'] = $plugin_id;
      }
    }
    $this->setConfiguration([
      'gutenberg_processors' => $processors_config,
    ]);
  }

  /**
   * Sets the Gutenberg processors.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function setHtmlGutenbergProcessors() {
    if (empty($this->htmlGutenbergProcessors)) {
      // Load and process gutenberg processor plugins.
      $processors = $this->getGutenbergProcessorManager()->getSortedDefinitions();
      foreach ($processors as $plugin_id => $plugin_definition) {
        $processor_config = $this->configuration['gutenberg_processors'][$plugin_id] ?? [];
        $this->htmlGutenbergProcessors[$plugin_id] = $this->getGutenbergProcessorManager()->createInstance($plugin_id, $processor_config);
      }
    }
  }

  /**
   * Returns the Gutenberg processor manager.
   *
   * @return \Drupal\html_processors\HtmlGutenbergProcessorManager
   *   The Gutenberg processor manager.
   */
  protected function getGutenbergProcessorManager() {
    return $this->htmlGutenbergParser ? $this->htmlGutenbergParser->getGutenbergProcessorManager() : \Drupal::service('plugin.manager.html_gutenberg_processor');
  }

}
