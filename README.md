# HTML Processors

This module provides some useful processors to convert HTML to Drupal,
it does that process through feeds tampers and Drupal actions,
the following are the initial features:

- Action: HTML to Gutenberg -> Converts node body field HTML to Gutenberg.
- Tamper: Href to Media -> Finds all links and detect media elements to import
  into the site, then it replaces the url to the Drupal file.
- Tamper: HTML to Gutenberg

For a full description of the module, visit the
[project page](https://www.drupal.org/project/html_processors).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/html_processors).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

You can create and configure the action to Convert body to Gutenberg on:
`_/admin/config/system/actions_`


## Maintainers

- Ivan Duarte - [jidrone](https://www.drupal.org/u/jidrone)