<?php

namespace Drupal\html_processors_tampers\Plugin\Tamper;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tamper\TamperableItemInterface;
use Drupal\tamper\TamperBase;
use Drupal\html_processors\Service\MediaGenerator;
use Drupal\html_processors\Service\HrefToMediaReplacer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation for replace url to file url.
 *
 * @Tamper(
 *   id = "href_to_media",
 *   label = @Translation("Href to Media"),
 *   description = @Translation("Download file, store it as media and return new url."),
 *   category = "Files"
 * )
 */
class HrefToMedia extends TamperBase implements ContainerFactoryPluginInterface {

  const REMOTE_SITE_URL = 'remote_url';
  const ONLY_RELATIVE = 'only_relative';

  /**
   * The media generator service.
   *
   * @var \Drupal\html_processors\Service\MediaGenerator
   */
  protected $mediaGenerator;

  /**
   * The href to media replacer.
   *
   * @var \Drupal\html_processors\Service\HrefToMediaReplacer
   */
  protected $hrefToMediaReplacer;

  /**
   * Constructs a new HrefToMedia instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\html_processors\Service\MediaGenerator $media_generator
   *   The media generator service.
   * @param \Drupal\html_processors\Service\HrefToMediaReplacer $href_to_media_replacer
   *   The href to media replacer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MediaGenerator $media_generator, HrefToMediaReplacer $href_to_media_replacer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $configuration['source_definition']);
    $this->mediaGenerator = $media_generator;
    $this->hrefToMediaReplacer = $href_to_media_replacer;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('html_processors.media_generator'),
      $container->get('html_processors.href_to_media_replacer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config[self::REMOTE_SITE_URL] = '';
    $config[self::ONLY_RELATIVE] = FALSE;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Remote site URL.
    $form[self::REMOTE_SITE_URL] = [
      '#type' => 'url',
      '#title' => $this->t('Remote site URL'),
      '#default_value' => $this->getSetting(self::REMOTE_SITE_URL),
      '#description' => $this->t('The remote site URL without trailing "/"'),
      '#required' => TRUE,
    ];
    // Only relative paths.
    $form[self::ONLY_RELATIVE] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only process relative paths'),
      '#default_value' => $this->getSetting(self::ONLY_RELATIVE),
      '#description' => $this->t('If checked the absolute paths will be skipped.'),
    ];
    // Media types paths.
    $form['media_paths'] = [
      '#type' => 'details',
      '#title' => $this->t('Media Paths'),
      '#open' => TRUE,
    ];
    foreach ($this->mediaGenerator->getMediaSettingsMap() as $media_type_id => $settings) {
      $form['media_paths'][$media_type_id] = [
        '#type' => 'textfield',
        '#title' => $this->t('Path to store files of type: @media_type', [
          '@media_type' => $settings['label'],
        ]),
        '#default_value' => $this->getSetting($media_type_id),
        '#description' => $this->t('Leave empty to use media type file directory.'),
        '#field_prefix' => 'public://',
        '#attributes' => [
          'placeholder' => $settings['file_directory'] ?? '',
        ],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $media_paths = $form_state->getValue('media_paths');
    $this->setConfiguration([
      self::REMOTE_SITE_URL => $form_state->getValue(self::REMOTE_SITE_URL),
      self::ONLY_RELATIVE => $form_state->getValue(self::ONLY_RELATIVE),
    ] + $media_paths);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\File\Exception\FileException
   * @throws \InvalidArgumentException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function tamper($data, TamperableItemInterface $item = NULL) {
    $result = $this->hrefToMediaReplacer->process($data, $this->configuration);
    return $result;
  }

}
