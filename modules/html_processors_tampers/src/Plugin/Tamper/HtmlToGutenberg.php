<?php

namespace Drupal\html_processors_tampers\Plugin\Tamper;

use Drupal\tamper\TamperableItemInterface;
use Drupal\tamper\TamperBase;
use Drupal\html_processors\Service\HtmlGutenbergParser;
use Drupal\html_processors\HtmlGutenbergProcessorConfigTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation to convert HTML to Gutenberg.
 *
 * @Tamper(
 *   id = "html_to_gutenberg",
 *   label = @Translation("HTML to Gutenberg"),
 *   description = @Translation("Converts HTML to Gutenberg."),
 *   category = "Text"
 * )
 */
class HtmlToGutenberg extends TamperBase implements ContainerFactoryPluginInterface {

  use HtmlGutenbergProcessorConfigTrait;

  /**
   * The HTML to Gutenberg parser.
   *
   * @var \Drupal\html_processors\Service\HtmlGutenbergParser
   */
  protected $htmlGutenbergParser;

  /**
   * Constructs a new SocialLinksBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\html_processors\Service\HtmlGutenbergParser $html_gutenberg_parser
   *   The HTML to Gutenberg parser.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, HtmlGutenbergParser $html_gutenberg_parser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $configuration['source_definition']);
    $this->htmlGutenbergParser = $html_gutenberg_parser;
    $this->setHtmlGutenbergProcessors();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('html_processors.html_gutenberg_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    return $config;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function tamper($data, TamperableItemInterface $item = NULL) {
    $processors_config = $this->configuration['gutenberg_processors'];
    $result = $this->htmlGutenbergParser->parse($data, $processors_config);
    return $result;
  }

}
