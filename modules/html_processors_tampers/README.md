## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Provides feeds tampers to convert HTML to Drupal.

## REQUIREMENTS

* Feeds.
* Feeds Tampers.

## INSTALLATION

  * Install normally as other modules are installed. For support:
    https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules


## CONFIGURATION

  * Configure the provided tampers as any other tamper.

## MAINTAINERS

Current maintainers:
 * Ivan Duarte (jidrone) - https://www.drupal.org/u/jidrone
